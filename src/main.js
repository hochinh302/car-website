import { createApp } from "vue";
import App from "./App.vue";
import router from "./routes";
import "./assets/libs/bootstrap/css/bootstrap.min.css";
import "./assets/libs/fontawesome/css/fontawesome-all.min.css";
import "./assets/libs/linearicons/linearicons.css";
import "./assets/css/rentnow-icons.css";
import "./assets/libs/flatpickr/flatpickr.min.css";
import "./assets/css/magnific-popup.css";
import "./assets/css/style.css";

// add js file
// import "./assets/js/jquery.min.js";
// import "./assets/js/popper.min.js";
// import "./assets/libs/bootstrap/js/bootstrap.min.js";
// import "./assets/libs/flatpickr/flatpickr.min.js";
// import "./assets/js/starrr.min.js";
// import "./assets/js/jquery.magnific-popup.min.js";
// import "./assets/js/scripts.js";
const app = createApp(App);

app.use(router);
app.mount("#app");
