import { createRouter, createWebHashHistory } from "vue-router";
import Home from "../views/AppHome.vue";
import About from "../views/AboutPage.vue";
import Login from "../views/AppLogin.vue";
import Register from "../views/AppRegister.vue";
import Service from "../views/ServicePage.vue";

import DefaultLayout from "../layouts/DefaultLayout.vue";
import OnlyLayout from "../layouts/OnlyLayout.vue";

const routes = [
  {
    path: "/",
    name: "default",
    component: DefaultLayout,
    redirect: "/",
    children: [
      { path: "/", name: "default", component: Home },
      { path: "/home", name: "home", component: Home },
      { path: "/about", name: "about", component: About },
      { path: "/service", name: "service", component: Service },
    ],
  },
  {
    path: "/auth",
    name: "only",
    component: OnlyLayout,
    redirect: "/login",
    children: [
      { path: "/login", name: "login", component: Login },
      { path: "/register", name: "register", component: Register },
      { path: "/home", name: "home", component: Home },
    ],
  },

  { path: "/:pathMatch(.*)*", component: About },
];

const router = createRouter({
  // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
  mode: "history",
  history: createWebHashHistory(),
  routes, // short for `routes: routes`
});

router.beforeEach(async (to) => {
  const isAuthenticated = localStorage.getItem("isLogin");
  if (
    // make sure the user is authenticated
    !isAuthenticated &&
    // ❗️ Avoid an infinite redirect
    to.name !== "login"
  ) {
    // redirect the user to the login page
    return { name: "login" };
  }
});

export default router;
